import React from 'react';

const ContactForm = ({
  name,
  setName,
  phone,
  setPhone,
  email,
  setEmail,
  handleSubmit
}) => {
  return (
    <form className='AddContactForm' onSubmit={handleSubmit}>

      <label>
        <input
          type='text'
          name='name'
          placeholder='contact name'
          value={name}
          onChange={(e) => setName(e.target.value)}
          required
        />
      </label>
      <br />

      <label>
        <input
          type='email'
          name='email'
          placeholder='contact email'
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </label>
      <br />

      <label>
        <input
          type='tel'
          name='phone'
          placeholder='contact phone'
          value={phone}
          onChange={(e) => setPhone(e.target.value)} 
          //title='International phone number check'
          pattern="^(\+[1-9][0-9]*(\([0-9]*\)|-[0-9]*-))?[0]?[1-9][0-9\- ]*$"
          required
        />
      </label>

      <br />

        <input 
            type='submit' 
            value='Add Contact' 
        />

    </form>
  );
};

export default ContactForm;