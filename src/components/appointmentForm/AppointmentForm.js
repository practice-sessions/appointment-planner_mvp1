import React from "react";

import ContactPicker from '../contactPicker/ContactPicker';

const AppointmentForm = ({

  contacts,
  title,
  setTitle,
  contact,
  setContact,
  date,
  setDate,
  time,
  setTime,
  handleSubmit
}) => {
  const getTodayString = () => {
    const [month, day, year] = new Date()
      .toLocaleDateString("en-US")
      .split("/");
    return `${year}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
  };

  const getContactPerson = () => {
    return contacts.map((contact) => contact.name)
  }

  return (
    <form className='AddAppointmentForm' onSubmit={handleSubmit}>

      <label>
        <input 
          type='text'
          name='title'
          value={title}
          placeholder='title'
          onChange={(e) => setTitle(e.target.value)}
          required
        />
      </label>
      <br />

      <label>
        <ContactPicker
          name='contact'
          value={contact} 
          contacts={getContactPerson()}
          placeholder='Appointment With'
          onChange={(e) => setContact(e.target.value)}
          //required
        />
      </label>
      <br />

      <label>
        <input 
          type='date'
          name='date'
          value={date}
          min={getTodayString()}
          placeholder='date'
          onChange={(e) => setDate(e.target.value)}
          required
        />
      </label>
      <br />

      <label>
        <input 
          type='time'
          name='time'
          value={time}
          placeholder='time'
          onChange={(e) => setTime(e.target.value)}
          required
        />
      </label>
      <br />

      <input 
          type='submit' 
          value='Add Appointment' 
        />


    </form>
  );
};

export default AppointmentForm;